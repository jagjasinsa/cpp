#include<bits/stdc++.h>
using namespace std;

int count_bits(int a, int b)
{
	int temp = a^b;
	int n = sizeof(int)*8;
	
	int count =0;
	for(int i=0; i<n; i++)
	{
		count += temp&1;
		temp >>= 1;
	}
	
	return count;
}

int main()
{
	
	int a,b;
	cout << "Mention the numbers" << endl;
	
	cin >> a >> b;
	
	cout << count_bits(a,b) << endl;
	
	return 0;
}
