#include <bits/stdc++.h>
using namespace std;

int compute_path(vector<vector<int>> &arr)
{
	int m = arr.size();
	int n = arr[0].size();
	 
	int dp[m][n];
	
	//bc
	dp[m-1][n-1] = arr[m-1][n-1] > 0 ? 1 : abs(arr[m-1][n-1]) + 1;
	
	//filling the last coloumn
	for(int i = m-2; i>=0; i--)
	{
		dp[i][n-1] = max(1, dp[i+1][n-1]-arr[i][n-1]);
	}
	
	//filling the last row
	for(int j=n-2; j>=0; j--)
	{
		dp[m-1][j] = max(1,dp[m-1][j+1]-arr[m-1][j]);
	}
	
	//now filling the dp[][] in bottom up fashion
	for(int i=m-2; i>=0; i--)
	{
		for(int j=n-2; j>=0; j--)
		{
			int where_to_go_points = min(dp[i][j+1], dp[i+1][j]);
			dp[i][j] = max(1, where_to_go_points-arr[i][j]);
		}
	}
	return dp[0][0];
}

int main()
{
	
	int a[3][3] = {{-2 ,-3, 3},
			 {-5, -10, 1},
			 {10, 30, -5}};
			 
	vector<vector<int>> arr;
	for(int i=0; i<3; i++)
	{
		vector<int> temp;
		for(int j=0; j<3; j++)
		{
			temp.push_back(a[i][j]);
		}
		arr.push_back(temp);
	}
	
	int output = compute_path(arr);
	cout << "Answer: " << output << endl;
	
	return 0;	
}
