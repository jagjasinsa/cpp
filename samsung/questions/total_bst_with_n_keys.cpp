#include <bits/stdc++.h>
using namespace std;

//This is the catalan number Cn = (2n)! / [(n+1)!*n!]

int findCatlen(int n)
{
	//Cn = [2n C n]/(n+1)
	
	int res = 1;
	
	for(int i=0; i < n; i++)
	{
		res = res*(2*n-i);
		res = res/(i+1);
	}
	return res/(n+1);
}


int main()
{
	
	cout << "Please mention the keys" << endl;
	int n;
	cin >> n;
	
	cout << findCatlen(n) << endl;
	
	return 0;
}
