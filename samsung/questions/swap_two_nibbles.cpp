#include <bits/stdc++.h>
using namespace std;

unsigned int swap_nibble(unsigned int num)
{
	return ( (num&0X0F)<<4 | (num&0XF0)>>4 );
}

int main()
{
	cout << "Number:: " << endl;
	unsigned int num;
	cin >> num;
	cout << swap_nibble(num) << endl;
}
