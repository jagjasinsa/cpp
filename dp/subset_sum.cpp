#include <bits/stdc++.h>
using namespace std;

bool do_magic(int arr[], int size, int sum)
{
	/*
		dp[i][j] = dp[i-1][j] || dp[i-1][sum-a[i]]
	*/
	//dp[i][j] i = till i is there exists a sum j ??
	bool dp[size+1][sum+1];
	
	//if the sum is zero and then the answer is true always
	for(int i=0; i<size+1; i++)
	{
		dp[i][0] = true;
	}
	
	//if the size is 0 and sum is not zero then the answer is false always
	for(int i=1; i<sum+1; i++)
	{
		dp[0][i] = false;
	}
	
	for(int count = 1; count <=sum; count++)
	{
		for(int i=1; i<=size; i++)
		{
			if(count<arr[i-1]) dp[i][count] = dp[i-1][count];
			if(count>=arr[i-1]) dp[i][count] = dp[i-1][count-arr[i-1]];
		}
	}
	return dp[size][sum];
}

int main()
{
	int arr[] = {1,2,3};
	int sum = 7;
	int n = 3;
	if(do_magic(arr,n,sum)) cout << "YO!!";
	else cout << "I WILL TRY AGAIN!!";
	return 0;
}
