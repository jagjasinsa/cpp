#include<bits/stdc++.h>
using namespace std;

class edge
{
	public:
		char a;
		char b;
		int w;
		edge(char aa, char bb, int ww)
		{
			a = aa;
			b = bb;
			w = ww;
		};
};

char find(char a, unordered_map<char,char> parent)
{
	if(parent[a]==a)
		return a;
	char p = find(parent[a],parent);
	parent[a] = p;
	return p;
}

void union_set(char set1, char set2, unordered_map<char,char> parent, unordered_map<char,int> rank)
{
	if(rank[set1]>rank[set2]) parent[set2] = set1;
	else if(rank[set1]<rank[set2]) parent[set1] = set2;
	else
	{
		parent[set1] = set2;
		rank[set2]++;
	}
}

int main()
{
	cout << "Mention the edges and their weight. Like: a b 234. Press 9999 to exit" << endl;
	char a,b;
	int w;
	cin >> a >> b >>w;
	vector<edge> data;
	while(w!=9999)
	{
		if(a<b)
		{
			edge e1(a,b,w);
			data.push_back(e1);
		}		
		else
		{
			edge e1(b,a,w);
			data.push_back(e1);
		}		
		cin >> a >> b >> w;
	}
	
	set<char> vertex_set;
	for(int i=0; i<data.size(); i++)
	{
		vertex_set.insert(data[i].a);
		vertex_set.insert(data[i].b);
	}
	
	unordered_map<char,char> parent;
	unordered_map<char,int> rank;
	
	for(auto i = vertex_set.begin(); i!= vertex_set.end(); i++)
	{
		parent.insert(make_pair(*i,*i));
	}
	
	for(auto i  = vertex_set.begin(); i!= vertex_set.end(); i++)
	{
		rank.insert(make_pair(*i,0));
	}
	
	
	
	return 0;
}
