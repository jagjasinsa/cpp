#include <bits/stdc++.h>
using namespace std;

void extract_k_largest(vector<int> &arr,int k)
{
	make_heap(arr.begin(), arr.end());
	for(int i=0; i<k; i++)
	{
		cout << arr[i] << " ";
	}
}

int main()
{
	
	int size;
	cout << "Please mention the size of the array" << endl;
	cin >> size;
	vector<int> arr(size);
	
	cout << "Please mention the elements of the array" << endl;
	for(int i=0; i<size; i++)
	{
		int temp;
		cin >> temp;
		arr.push_back(temp);
	}
	
	cout << "Please mention the value of k" << endl;
	int k;
	cin >> k;
	extract_k_largest(arr,k);
	
	return 0;
}
