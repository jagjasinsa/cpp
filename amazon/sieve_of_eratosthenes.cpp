#include <bits/stdc++.h>
using namespace std;

void do_magic(int n)
{
	bool arr[n+1];
	for(int i=0; i<n+1; i++)
	{
		arr[i] = true;
	}
	arr[0]= false;
	arr[1]=false;
	arr[2]= true;
	for(int i=0; i<n+1; i++)
	{
		if(arr[i]==true)
		{
			cout << i << " -----";
			int j=2;
			while(1)
			{
				if(i*j>n)
				{
					break;
				}
				else
				{
					arr[i*j]=false;
					j++;
				}
			}			
		}
	}
}


int main()
{
	
	int n = 10;
	do_magic(n);	
	return 0;
}
