#include <bits/stdc++.h>
using namespace std;

bool isPalindromic(string str, int start, int end)
{
	while(start<end)
	{
		if(str[start]!=str[end])
		{
			return false;
		}
		start++;
		end--;
	}
	return true;
}

void util(vector<vector<string>> &all, vector<string> curr, int start, int n, string str)
{
	if(start>=n)
	{
		all.push_back(curr);
		return;
	}
	
	for(int i=start; i<n; i++)
	{
		if(isPalindromic(str, start, i))
		{
			curr.push_back(str.substr(start, i-start+1));
			util(all, curr, i+1, n, str);
			curr.pop_back();
		}
	}
}


void do_magic(string str)
{
	int n = str.length();
	vector<vector<string>> all;
	vector<string> curr;
	
	util(all,curr,0,n,str);
	
	for(int i=0; i<all.size(); i++)
	{
		for(int j=0; j<all[i].size(); j++)
		{
			cout << all[i][j] << " ";
		}
		cout << "\n";
	}
	
}

int main()
{
	string str = "missssim";
	do_magic(str);	
	return 0;
}
