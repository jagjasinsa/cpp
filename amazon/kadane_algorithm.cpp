#include <bits/stdc++.h>
using namespace std;

int do_magic(int arr[], int n)
{
	
	int max_so_far =INT_MIN;
	int max_end_here = 0;
	for(int i=0;i<n;i++)
	{
		max_end_here += arr[i];
		if(max_so_far < max_end_here)
		{
			max_so_far = max_end_here;
		}
		if(max_end_here<0) max_end_here = 0;
	}
	return max_so_far;
}



int main()
{
	int arr[] = {-2,-3,4,-1,-2,1,5,-3};
	int n = sizeof(arr)/sizeof(int);
	cout << do_magic(arr,n);
	
	return 0;
}
