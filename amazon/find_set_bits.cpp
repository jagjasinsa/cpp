#include <bits/stdc++.h>
using namespace std;

bool isKthBitSet(int x, int k)
{
	return (x&(1<<k))?true:false;
}

int main()
{
	cout << "Mention the number" << endl;
	int number;
	cin >> number;
	
	int bits = sizeof(int)*8;
	int output = 0;
	for(int i=0; i<bits; i++)
	{
		if(isKthBitSet(number,i))
		output++;
	}
	cout << "Answer: " << output << endl;
	
	
	return 0; 
}
