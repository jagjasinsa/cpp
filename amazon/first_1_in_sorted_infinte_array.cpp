// C++ implementation to find the index of first 1
// in an infinite sorted array of 0's and 1's
#include <bits/stdc++.h>
using namespace std;

void do_magic(vector<int> arr, int left, int right,int *pos)
{
	if(left<right)
	{
		int mid = (left+right)/2;
		if(arr[mid]==1)
		{
			if(mid<*pos) *pos = mid;
		}
		do_magic(arr,mid+1,right,pos);
		do_magic(arr,left, mid, pos);
	}
}

int main()
{
	cout << "Please mention the size of the array" << endl;
	int size;
	cin >> size;
	
	cout<< "Please mention the numbers(0 or 1)" << endl;
	
	vector<int> arr;
	for(int i=0;i<size; i++)
	{
		int temp;
		cin >> temp;
		arr.push_back(temp);
	}
	
	//sort(arr.begin(),arr.end());
	int pos=size;
	do_magic(arr,0,arr.size()-1,&pos);
	cout << pos << endl;
	return 0;
}
