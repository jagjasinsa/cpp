#include <bits/stdc++.h>
using namespace std;

int main()
{
    cout << "Please mention the number" << endl;
    int num;
    cin >> num;

    string s[100];
    int i=0;

    while(num>0)
    {
        int rem = num%26;

        if(rem==0)
        {
            s[i++] = 'Z';
            num = num/26 - 1;
        }
        else
        {
            s[i++] = (rem-1) + 'A';
            num = num/26; 
        }
    }
    s[i] = '\n';
    for(int j=i-1; j>=0; j--) cout << s[j];


    return 0;
}