#include <bits/stdc++.h>
using namespace std;

bool sort_coloumn_wise(vector<int> &v1, vector<int> &v2)
{
    return v1[0]>v2[0];
}

int main()
{

    int come[] = {1,2,9,5,5};
    int go[] = {4,5,12,9,12};

    vector<vector<int> > arr;
    for(int i=0; i<sizeof(come)/sizeof(int); i++)
    {
        vector<int> temp;
        temp.push_back(come[i]);
        temp.push_back(1);
        arr.push_back(temp);
    }

    for(int i=0; i<sizeof(go)/sizeof(int); i++)
    {
        vector<int> temp;
        temp.push_back(go[i]);
        temp.push_back(-1);
        arr.push_back(temp);
    }

    sort(arr.begin(), arr.end(), sort_coloumn_wise);

    int max = INT_MIN;
    int pos = 0;
    int curr = 0;
    for(int i=0; i<arr.size(); i++)
    {
        if(arr[i][1]==1)
        {
            curr += 1;
            if(max<curr)
            {
                max = curr;
                pos = i;
            }
        }
        else{
            curr -= 1;
            if(max<curr)
            {
                max = curr;
                pos = i;
            }
        }
    }

    cout << "Time: " << pos << endl;

    return 0;
}
