#include <bits/stdc++.h>
using namespace std;

struct node
{
    int value;
    struct node *left;
    struct node *right;
};
struct node *root;

struct node* create_tree(struct node *a, int data)
{
    if(a==NULL)
    {
        a = (struct node*)malloc(sizeof(struct node));
        a->value = data;
        a->left = NULL;
        a->right = NULL;
    }
    else if(data > a->value)
    {
        struct node *temp = a;
        a->right = create_tree(temp->right, data);
    }
    else
    {
        struct node *temp = a;
        a->left = create_tree(temp->left, data);
    }
    return a;
}

void bst_to_dll(struct node *root, struct node *head)
{
    if(root!=NULL)
    {
        static struct node *prev = NULL;
        bst_to_dll(root->left,head);
        
        if(prev==NULL)
        {
            head = root;
        }
        else{
            root->left = prev;
            prev->right = root;
        }
        prev = root;

        bst_to_dll(root->right,head);
    }
}

void print_dll(struct node *head)
{
    struct node *temp = head;
    while(temp!=NULL)
    {
        cout << temp->value << " ";
        temp = temp->right;
    }
}

int main()
{
    root = NULL;

    int temp;
    cout << "Enter data. -1 to exit" << endl;
    cin >> temp;

    while(temp!=-1)
    {
        root = create_tree(root, temp);
        cin >> temp;
    }

    struct node *head;
    bst_to_dll(root,head);

    print_dll(root);

    return 0;

}